import { Component } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { WeatherModel } from './weather.model';
import { WeatherForecastModel } from './weather_forecast.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  openWeatherMap: WeatherModel;
  weatherForecast: WeatherForecastModel;
  userPosition: any;
  imgSrc: 'http://openweathermap.org/img/w/';

  constructor(private httpClient: HttpClient) {
  }

  ngOnInit() {
  this.findMe();
}

findMe() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition((position) => {
      this.userPosition = position;
      this.showWeather(position);
      this.showForecast(position);
    });
  } else {
    alert("Geolocation is not supported by this browser.");
  }
}

showWeather(position) {
  let url = `https://api.openweathermap.org/data/2.5/weather?lat=${position.coords.latitude}&lon=${position.coords.longitude}&units=imperial&APPID=933d14048dbe9fd78750c4958c987328`;
  this.httpClient.get<WeatherModel>(url).subscribe(data => {
    this.openWeatherMap = data;
  })
}
showForecast(position) {
  let url = `https://api.openweathermap.org/data/2.5/forecast?lat=${position.coords.latitude}&lon=${position.coords.longitude}&units=imperial&APPID=933d14048dbe9fd78750c4958c987328`;
  this.httpClient.get<WeatherForecastModel>(url).subscribe(data => {
    console.log(data);
    this.weatherForecast = data;
  })
}
refresh() {
    this.showWeather(this.userPosition);
  }
  addZip(value){
    console.log(value);

    // api.openweathermap.org/data/2.5/weather?q=London,uk&APPID=933d14048dbe9fd78750c4958c987328  weather?zip=
  }
}
