export class WeatherForecastModel {
  list: any;
  coord: {
    lon: number,
    lat: number
  };
  sys: {
    sunrise: number,
    sunset: number
  };
  weather: {
    main: string,
    description: string,
    icon: string
  };
  main: {
    temp: number,
    humidity: number,
    pressure: number,
    temp_min: number,
    temp_max: number,
  };
  wind: {
    speed: number,
    degree: number
  };
  clouds: {
    all: number
  };
  dt: Date;
  name: string;
}
